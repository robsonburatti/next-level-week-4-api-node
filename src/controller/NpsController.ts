import { Request, Response } from 'express';
import { getCustomRepository, Not, IsNull } from 'typeorm';
import { SurveysUserRepository } from '../repositories/SurveysUserRepository';

class NpsController {

    /**
     * 1 2 3 4 5 6 7 8 9 10
     * Detratores => 1 - 6
     * Passivos => 7 - 8
     * Promotores => 9 - 10
     * 
     * (((Promotores - Detratores) / (Número de respondentes)) * 100)
     */
    async execute(request: Request, response: Response) {
        const { surveys_id } = request.params;

        const surveysUserRepository = getCustomRepository(SurveysUserRepository);

        const surveysUser = await surveysUserRepository.find({
            surveys_id,
            value: Not(IsNull())
        });

        const detractors = surveysUser.filter((survey) => 
            survey.value >= 0 && survey.value <= 6
        ).length;

        const promoters = surveysUser.filter(survey => {
            return survey.value >= 9 && survey.value <= 10;
        }).length;

        const passives = surveysUser.filter(survey => {
            return survey.value == 7 || survey.value == 8;
        }).length;

        const totalAnswers = surveysUser.length;

        const calculate = Number(
            (((promoters - detractors) / (totalAnswers)) * 100).toFixed(2)
        );

        return response.status(200).json({
            detractors,
            promoters,
            passives,
            totalAnswers,
            nps: calculate
        });
    }
}

export { NpsController };
