import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import { resolve } from 'path';
import { SurveysRepository } from '../repositories/SurveysRepository';
import { SurveysUserRepository } from '../repositories/SurveysUserRepository';
import { UserRepository } from '../repositories/UserRepository';
import SendMailService from '../services/SendMailService';
import AppError from '../errors/AppError';

class SendMailController {
    
    async execute(request: Request, response: Response) {
        const { email, surveys_id } = request.body;

        const userRepository = getCustomRepository(UserRepository);
        const surveysRepository = getCustomRepository(SurveysRepository);
        const surveysUserRepository = getCustomRepository(SurveysUserRepository);

        const user = await userRepository.findOne({
            email
        });

        if (!user) {
            throw new AppError('User does not exists');
        }

        const surveys = await surveysRepository.findOne({
            id: surveys_id
        });

        if (!surveys) {
            throw new AppError('Surveys does not exists');
        }

        const surveysUserAlreadyExists = await surveysUserRepository.findOne({
            where: {
                user_id: user.id,
                value: null
            },
            relations: ['user', 'surveys']
        });

        const variables = {
            name: user.name,
            title: surveys.title,
            description: surveys.description,
            link: process.env.URL_MAIL,
            id: ''
        };

        const npsPath = resolve(__dirname, '..', 'views', 'emails', 'npsMail.hbs');

        if (surveysUserAlreadyExists) {
            variables.id = surveysUserAlreadyExists.id;
            await SendMailService.execute(email, surveys.title, variables, npsPath);
            return response.status(201).json(surveysUserAlreadyExists);
        }

        const surveysUser = surveysUserRepository.create({
            user_id: user.id,
            surveys_id
        });

        await surveysUserRepository.save(surveysUser);

        variables.id = surveysUser.id;

        await SendMailService.execute(email, surveys.title, variables, npsPath);
        
        return response.status(201).json(surveysUser);
    }

    async show(request: Request, response: Response) {
        const surveysUserRepository = getCustomRepository(SurveysUserRepository);

        const all = await surveysUserRepository.find();

        return response.status(200).json(all);
    }
}

export { SendMailController };
