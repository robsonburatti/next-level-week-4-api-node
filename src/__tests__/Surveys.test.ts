import request from 'supertest';
import { getConnection } from 'typeorm';
import { app } from '../app';
import createConnection from '../database';

describe('Surveys', () => {

    beforeAll(async () => {
        const connection = await createConnection();
        await connection.runMigrations();
    });

    afterAll(async () => {
        const connection = await getConnection();
        await connection.dropDatabase();
        await connection.close();
    });

    it('Should be able to create a new surveys', async () => {
        const response = await request(app)
            .post('/surveys')
            .send({ 
                title: 'Test Surveys',
                description: 'test surveys description'
            });
        
        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
    });
    
    it('Should be able to get all surveys', async () => {
        await request(app)
            .post('/surveys')
            .send({ 
                title: 'Test Surveys 2',
                description: 'test surveys description 2'
            });
        
        const response = await request(app)
            .get('/surveys');
        
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(2);
    });

});