import request from 'supertest';
import { getConnection } from 'typeorm';
import { app } from '../app';
import createConnection from '../database';

describe('Users', () => {

    beforeAll(async () => {
        const connection = await createConnection();
        await connection.runMigrations();
    });

    afterAll(async () => {
        const connection = await getConnection();
        await connection.dropDatabase();
        await connection.close();
    });

    it('Should be able to create a new user', async () => {
        const response = await request(app)
            .post('/users')
            .send({ 
                name: 'User test',
                email: 'user@test.com'
            });
        
        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
    });

    it('Should not be able to create a user with exists email', async () => {
        const response = await request(app)
            .post('/users')
            .send({ 
                name: 'User test',
                email: 'user@test.com'
            });
        
        expect(response.status).toBe(400);
    });


    it('Should be able to get all users', async () => {
        await request(app)
            .post('/users')
            .send({ 
                name: 'User test 2',
                email: 'user@test2.com'
            });
        
        const response = await request(app)
            .get('/users');
        
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(2);
    });

});